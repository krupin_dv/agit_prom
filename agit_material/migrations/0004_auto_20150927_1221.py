# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0003_auto_20150904_1624'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agitmaterial',
            name='file',
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='image',
            field=models.FileField(default='', upload_to=b'agit_images', verbose_name='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u0435'),
            preserve_default=False,
        ),
    ]
