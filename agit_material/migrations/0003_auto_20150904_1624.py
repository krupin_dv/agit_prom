# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0002_picture'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='picture',
            name='height',
        ),
        migrations.RemoveField(
            model_name='picture',
            name='width',
        ),
        migrations.AlterField(
            model_name='picture',
            name='image',
            field=models.FileField(upload_to=b'pictures'),
        ),
    ]
