from django.http import HttpResponse
from django.shortcuts import render_to_response
from  django.views.generic import FormView, ListView, DetailView
from  django.views.generic.edit import FormMixin
# from django.views.decorators.csrf import csrf_exempt

from . forms import SearchForm
from . models import AgitMaterial, Picture


class AgitMaterialListView(ListView):
    queryset = AgitMaterial.objects.all()
    paginate_by = 10

class AgitMaterialDetailView(DetailView):
    model = AgitMaterial






class AgitMaterialSearchMixin(object):
    def agit_search(self, text):
        query = u'@(title,tags,description,groups) %s' % text
        return AgitMaterial.search.query(query)


class PictureSearchMixin(object):
    def picture_search(self, text):
        query = u'@(title,tags) %s' % text
        return Picture.search.query(query)



class SearchView(ListView, AgitMaterialSearchMixin, PictureSearchMixin):
    template_name = 'agit_material/search.html'
    form_class = SearchForm
    paginate_by = 10

    def get(self, request):
        self.form = self.form_class(request.GET)
        self.object_list = self.get_queryset() # for post method
        return super(SearchView, self).get(request)

#    def post(self, request):
#        # self.form = self.get_form(self.form_class)
#        # self.object_list = self.get_queryset()
#        return self.render_to_response(self.get_context_data())

    def get_queryset(self):
        # qs = AgitMaterial.objects.none()
        qs = AgitMaterial.objects.order_by('-id')[:self.paginate_by] # latest

        if self.form.is_valid():
            # Sphinx search
            text = self.form.cleaned_data['text']
            if text:
                qs = self.agit_search(text)
        else:
            print self.form.errors

        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(SearchView, self).get_context_data(*args, **kwargs)
        context['form'] = self.form
        if self.form.is_valid():
            text = self.form.cleaned_data['text']
            if text:
                context['query_string'] = 'text=%s' % text
                context['pictures'] = self.picture_search(text)[0:10]
            else:
                 # just show latest pictures
                context['pictures'] = Picture.objects.order_by('-id')[:self.paginate_by] # latest

        return context


# not used
class SearchViewFormView(FormView):
    template_name = 'agit_material/search.html'
    form_class = SearchForm
    success_url = '/'

    def form_valid(self, form):
        text = form.cleaned_data['text'] # text to search
#       print(text)
#        results = AgitMaterial.search.query(text)
#        print results.count()
#        for a in results:
#            print a.title
        query = u'@(title,tags,description) ' + text
        results = AgitMaterial.search.query(query)

        context = {'form': form, }
        return self.render_to_response(context)
        # return super(SearchView, self).form_valid(form)





def test(request):
    print("test_________")

    #return render_to_response('agit_material/search.html')
    return HttpResponse("TEST!!!!")
