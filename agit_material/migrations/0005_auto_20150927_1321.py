# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0004_auto_20150927_1221'),
    ]

    operations = [
        migrations.CreateModel(
            name='TargetGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=64, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435')),
            ],
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='description',
            field=models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435', blank=True),
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='format',
            field=models.PositiveSmallIntegerField(blank=True, null=True, choices=[(1, b'A1'), (2, b'A2'), (3, b'A3'), (4, b'A4'), (5, b'A5'), (6, b'\xd0\x91\xd1\x83\xd0\xba\xd0\xbb\xd0\xb5\xd1\x82')]),
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='insctruction',
            field=models.TextField(verbose_name='\u0418\u043d\u0441\u0442\u0440\u0443\u043a\u0446\u0438\u044f \u043f\u043e \u0438\u0437\u0433\u043e\u0442\u043e\u0432\u043b\u0435\u043d\u0438\u044e', blank=True),
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='tags',
            field=models.CharField(default='', max_length=255, verbose_name='\u0422\u0435\u0433\u0438'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='type',
            field=models.PositiveSmallIntegerField(blank=True, null=True, choices=[(1, b'\xd0\xbf\xd0\xbb\xd0\xb0\xd0\xba\xd0\xb0\xd1\x82'), (2, b'\xd0\xbb\xd0\xb8\xd1\x81\xd1\x82\xd0\xbe\xd0\xb2\xd0\xba\xd0\xb0'), (3, b'\xd1\x80\xd0\xb0\xd1\x81\xd1\x82\xd1\x8f\xd0\xb6\xd0\xba\xd0\xb0'), (4, b'\xd0\xb1\xd0\xb0\xd0\xbd\xd0\xb5\xd1\x80')]),
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='target_group',
            field=models.ManyToManyField(help_text='\u0426\u0435\u043b\u0435\u0432\u0430\u044f \u0430\u0443\u0434\u0438\u0442\u043e\u0440\u0438\u044f', to='agit_material.TargetGroup'),
        ),
    ]
