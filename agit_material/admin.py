from django.contrib import admin

from . models import TargetGroup, File, AgitMaterial, Picture



class SphinxModelAdmin(admin.ModelAdmin):
    '''
        http://habrahabr.ru/post/188242/
    '''

    def get_search_results(self, request, queryset, search_term):
        if search_term:
            sphinx_queryset = self.model.search.query(search_term)
            doc_ids = [doc.pk for doc in sphinx_queryset]
            queryset = queryset.filter(pk__in=doc_ids)
            return queryset, True
        else:
            return super(SphinxModelAdmin, self).get_search_results(
                request, queryset, search_term
            )


class TargetGroupAdmin(admin.ModelAdmin):
    list_display = ['title', ]
    search_fields = ['title', ]

class FileInline(admin.TabularInline):
    model = File

class PictureInline(admin.TabularInline):
    model = Picture


class AgitMaterialAdmin(SphinxModelAdmin):
    list_display = ['title', ]
    search_fields = ['title', ]
    inlines = (FileInline, PictureInline,)


class PictureAdmin(admin.ModelAdmin):
    list_display = ['title', ]
    # exclude = ['agit_material', ]


admin.site.register(TargetGroup, TargetGroupAdmin)
admin.site.register(AgitMaterial, AgitMaterialAdmin)
admin.site.register(Picture, PictureAdmin)
