# -*- coding: utf-8 -*-
from django import forms



class SearchForm(forms.Form):
    text = forms.CharField(label=u'Введите фразу для поиска', max_length=50, required=False)


