# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0006_auto_20150927_1336'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='agitmaterial',
            name='files',
        ),
        migrations.RemoveField(
            model_name='agitmaterial',
            name='pictures',
        ),
        migrations.AddField(
            model_name='file',
            name='agit_material',
            field=models.ForeignKey(related_name='files', default=None, to='agit_material.AgitMaterial'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='picture',
            name='agit_material',
            field=models.ForeignKey(related_name='pictures', blank=True, to='agit_material.AgitMaterial', null=True),
        ),
    ]
