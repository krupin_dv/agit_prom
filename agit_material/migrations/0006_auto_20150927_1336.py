# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0005_auto_20150927_1321'),
    ]

    operations = [
        migrations.CreateModel(
            name='File',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.FileField(upload_to=b'agit_materail_files')),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435/\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439', blank=True)),
            ],
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='pictures',
            field=models.ForeignKey(default=None, blank=True, to='agit_material.Picture', help_text='\u0418\u0437\u043e\u0431\u0440\u0430\u0436\u0435\u043d\u0438\u044f'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='agitmaterial',
            name='image',
            field=models.FileField(upload_to=b'agit_materail_previews', verbose_name='\u041f\u0440\u0435\u0432\u044c\u044e'),
        ),
        migrations.AddField(
            model_name='agitmaterial',
            name='files',
            field=models.ForeignKey(default=None, to='agit_material.File', help_text='\u0424\u0430\u0439\u043b\u044b'),
            preserve_default=False,
        ),
    ]
