# -*- coding: utf-8 -*-
from django.core.exceptions import ValidationError
from djangosphinx.models import SphinxSearch

from django.db import models

FORMAT_CHOICES = (
    (1, 'A1'),
    (2, 'A2'),
    (3, 'A3'),
    (4, 'A4'),
    (5, 'A5'),
    (6, 'Буклет'),
)

TYPE_CHOICES = (
    (1, 'плакат'),
    (2, 'листовка'),
    (3, 'растяжка'),
    (4, 'банер'),
)


def file_is_image(file):
    file_name = file.file.name
    ext = file_name.split('.').pop().lower() # extension of file

    if ext not in ['jpg', 'png', 'svg']:
        raise ValidationError(u'%s is not image file. Please upload jpg, png or svg file' % file_name)




class TargetGroup(models.Model):
    title = models.CharField(u'Название', max_length=64)

    def __unicode__(self):
        return self.title



class File(models.Model):
    file = models.FileField(upload_to='agit_materail_files')
    description = models.TextField(u'Описание/Комментарий', blank=True)
    agit_material = models.ForeignKey('AgitMaterial', related_name="files")

    def file_name(self):
        return self.file.file.name.split("/").pop()


class Picture(models.Model):
    agit_material = models.ForeignKey('AgitMaterial', related_name="pictures", null=True, blank=True)

    title = models.CharField(u'Название', max_length=64)
    image = models.FileField(upload_to='pictures', validators=[file_is_image, ])
    tags = models.CharField(u'Теги', max_length=255)
    # width = models.PositiveSmallIntegerField(null=True, blank=True)
    # height = models.PositiveSmallIntegerField(null=True, blank=True)

    search = SphinxSearch(
        index='agit_material_pictures',
        weights={
            'title': 100,
            'tags': 100,
        },
        mode='SPH_MATCH_EXTENDED',
    )

    def __unicode__(self):
        return self.title

    def is_svg(self):
        file_name = self.image.file.name
        ext = file_name.split('.').pop().lower() # extension of file
        return ext == 'svg'


class AgitMaterial(models.Model):
    title = models.CharField(u'Название', max_length=64)
    image = models.FileField(u'Превью', upload_to='agit_materail_previews', validators=[file_is_image, ])
    tags = models.CharField(u'Теги', max_length=255)
    target_group = models.ManyToManyField(TargetGroup, help_text=u'Целевая аудитория')

    format = models.PositiveSmallIntegerField(choices=FORMAT_CHOICES, null=True, blank=True)
    type = models.PositiveSmallIntegerField(choices=TYPE_CHOICES, null=True, blank=True)

    description = models.TextField(u'Описание', blank=True)
    insctruction = models.TextField(u'Инструкция по изготовлению', blank=True)


    search = SphinxSearch(
        index='agit_material_agitmaterial',
        weights={
            'title': 100,
            'tags': 100,
            'groups': 95,
            'description': 90,

        },
        # mode='SPH_MATCH_ALL',
        # http://sphinxsearch.com/docs/latest/matching-modes.html
        # http://sphinxsearch.com/docs/latest/extended-syntax.html
        mode='SPH_MATCH_EXTENDED',
        # rankmode='SPH_RANK_NONE',
        # rankmode='SPH_RANK_MATCHANY', # not work
    )

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['id']
