# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('agit_material', '0007_auto_20150927_1406'),
    ]

    operations = [
        migrations.AddField(
            model_name='picture',
            name='tags',
            field=models.CharField(default='', max_length=255, verbose_name='\u0422\u0435\u0433\u0438'),
            preserve_default=False,
        ),
    ]
